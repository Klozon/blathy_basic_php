<footer class="text-center mt-5">
    <p>Copyright</p>
</footer>
</div>
<!-- SCRIPTS -->
<!-- Show / Hide Password -->

<script>
    let isShown = false; // Boolean, hogy tudjam éppen milyen állapotban van az ikon és a jelszó.
    function passwordVisibility(inputId, icon) {

        let icon1 = icon;
        let passwordInput = document.getElementById(inputId);
        if (!isShown) {
            icon1.setAttribute("class", "svg-inline--fa fa-eye-slash passwordIcon"); /* Átállítom a classt (****fontos: futásban a FontAwesome class máshogy néz ki, mint programozás során) */
            passwordInput.setAttribute("type", "text"); /* Átállítom a típust  */
            isShown = true; /* Jelzem a rendszernek, hogy látom a jelszót */
        }
        else {
            icon1.setAttribute("class", "svg-inline--fa fa-eye passwordIcon"); /* Visszaállítom az ikont**** */
            passwordInput.setAttribute("type", "password"); /* Visszaállítom a típust */
            isShown = false; /* Jelzem a rendszernek, hogy nem látom a jelszót */
        }
    }
    console.log("A konzolba írt Debug dolgok.");
</script>

</body>

</html>