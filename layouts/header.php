<?php

/* Elindítom a sessiont, hogy hkozzáférjek az oldalon a benne lévő dolgokhoz */
session_start();
?>
 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lionsdale Studio</title>

    <!-- Bootstrap links -->
    <link rel="stylesheet" href="../../Bootstrap/css/bootstrap.min.css">
    <script src="../../Bootstrap/popper/popper.js"></script>
    <script src="../../Bootstrap/js/bootstrap.min.js"></script>

    <!-- Saját css a keretrendszer után -->
    <link rel="stylesheet" href="../assets/style.css">

    <!-- Jquery input -->
    <script src="../assets/jquery-3.7.1.min.js"></script>

    <!-- Input Mask -->
    <script src="../assets/bootstrap-inputmask.min.js"></script>

    <!-- FontAwesome -->
    <link rel="stylesheet" href="../assets/FontAwesome/css/all.css">
    <script src="../assets/FontAwesome/js/all.js"></script>

    <!-- Parsley validator -->
    <script src="../assets\parsley.js"></script>
</head>

<body>

    <nav class="navbar navbar-expand-sm navbar-dark bg-primary p-2">
        <a class="navbar-brand" href="../views/index.php"><i class="fa-solid fa-computer fa-2xl" style="color: #fff;"></i></a>
        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse"
            data-bs-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false"
            aria-label="Toggle navigation"></button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav me-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" href="#" aria-current="page">Home <span
                            class="visually-hidden">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-bs-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Dropdown</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="#">Action 1</a>
                        <a class="dropdown-item" href="#">Action 2</a>
                    </div>
                </li>
            </ul>

            <?php
            if (isset($_SESSION['user'])) {
                /* Ha az állítás igaz, akkor be vagyok jelentkezve */
                echo '<form action="../controllers/logout.php" method="POST">';
                echo '<button class="btn btn-danger" type="submit" name="submit">Logout</button>';
                echo '</form>';

            } else {
                /* Ha az állítás hamis, akkor ki vagyok jelentkzve */
                echo '<form class="d-flex my-2 my-lg-0" action="../controllers/login.php" method="POST">';
                echo '<input name="username" class="form-control me-sm-2" type="text" placeholder="Username">';
                echo '<input name="password" class="form-control me-sm-2" type="text" placeholder="Password">';
                echo '<button class="btn btn-success my-2 my-sm-0" type="submit" name="submit">Login</button>';
                echo ' </form>';
                echo '<a href="../views/create_user.php" class="btn btn-warning" style="margin-left: 2%">Register</a>';
            }
            ?>
            
        </div>
    </nav>

    <div id="content">

