<?php

if (isset($_POST['submit'])) {

    /* Adatbázis */
    require "database.php";

    /* Defend SQL Injection */

    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);
    $passwordConfirm = mysqli_real_escape_string($connection, $_POST['passwordConfirm']);
    $username = mysqli_real_escape_string($connection, $_POST['username']);
    $birthdate = mysqli_real_escape_string($connection, $_POST['birthdate']);
    $fullname = mysqli_real_escape_string($connection, $_POST['fullname']);
    $postal = mysqli_real_escape_string($connection, $_POST['postal']);
    $country = mysqli_real_escape_string($connection, $_POST['country']);
    $street = mysqli_real_escape_string($connection, $_POST['street']);
    $omid = mysqli_real_escape_string($connection, $_POST['omid']);
    $nationality = mysqli_real_escape_string($connection, $_POST['nationality']);
    $language = mysqli_real_escape_string($connection, $_POST['language']);
    $phone = mysqli_real_escape_string($connection, $_POST['phone']);
    $sex = mysqli_real_escape_string($connection, $_POST['sex']);
    $confirm = mysqli_real_escape_string($connection, $_POST['confirm']);
    $address_id = 0;


    /* Létezik-e a USERNAME */
    $user_query = "SELECT id FROM users WHERE username = '$username'";
    $result = mysqli_query($connection, $user_query);

    if (mysqli_num_rows($result) > 0) {
        header("Location: ../views/create_user.php?error=usertaken");
        exit('Username is taken taken.');
    }

    /* Jelszó validálás */
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number = preg_match('@[0-9]@', $password);
    $specialChars = preg_match('@[^\w]@', $password);

    /* SQL Query for inserting */
    $sql_query = "INSERT INTO `users`(`name`, `username`, `password_id`, `birthdate`,
 `phone`, `email`, `address_id`, `omid`, `nationality_id`, `language_id`, `sex_id`) 
VALUES (?,?,?,?,?,?,?,?,?,?,?)";

    /* Statement initialization */
    $stmt = mysqli_stmt_init($connection);

    /* Validating 101 */
    /* Validate if something empty */
    $empty = array();
    foreach ($_POST as $key => $value) {
        if (empty($value)) {
            array_push($empty, $key);
        }
    }
    $emptyString = "?";

    foreach ($empty as $key => $value) {
        $emptyString .= "empty_" . $key . "=" . $value . "&";
    }

    if (count($empty) > 1) {
        echo $empty[0] . "<br>";
    } else if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) { /* E-mail validate */
        header("Location: ../views/create_user.php?error=email");
        echo "Bad email";
    } else if (
        /* Password strength validate */
        !$uppercase ||
        !$lowercase ||
        !$number ||
        !$specialChars ||
        strlen($password) < 8
    ) {
        echo 'Több karakter mindenféle védelemmel';
        header("Location: ../views/create_user.php?error=pweak");
    } else if ($password !== $passwordConfirm) { /* Matching password validate */
        header("Location: ../views/create_user.php?error=pwmatch");
        echo 'Nem egyező jelszavak';
    }

    /* JELSZÓ HASHELÉSE */
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    /* Jelszó beszúrása a jelszavak táblába. */
    $password_query = "INSERT INTO passwords (password) VALUES ('$hashedPassword')";
    mysqli_query($connection, $password_query);

    /* Az imént beszúrt PASSWORD ID-jének a lekérése */
    $get_pw_id_query = "SELECT MAX(id) FROM passwords";
    $result = mysqli_query($connection, $get_pw_id_query);
    $get_id = mysqli_fetch_assoc($result);
    $id = $get_id['MAX(id)'];


    /* ADDRESS VALIDATION ÉS ADDRESS BESZÚRÁSA / LEKÉRÉSE */
    /* Cím összes adatának lekérése kell a street alapján,
     ha van találat, akkor megnézed a lekért cím POSTAL kódját, 
     ha ugyanaz, mint az átkért, akkor megnézed a countryt. 
     Ha minden ugyanaz, akkor address_id=lekértid */

    $addressExists = false; //Létezik-e a címtáramban az új felhasználó címe. Az a feltételezésem, hogy nem.
    //megvizsgálom.
    $addressValidateQuery = "SELECT * FROM addresses WHERE street = '$street'"; //Megpróbálod lekérni a címet
    $result = mysqli_query($connection, $addressValidateQuery);
    if (mysqli_num_rows($result) != 0) { //Van eredmény
        $adatok = mysqli_fetch_assoc($result); //Akkor elmented az eredményt egy változóba
        if ($adatok['postal_code'] == $postal && $adatok['nationality_id'] == $nationality) { //Megnézed, hogy az irsz és az ország is egyezik-e.
            /* Az imént feltöltött address ID-jének a lekérése következik. */
            $address_id = $adatok['id']; //Akkor az átkérésnél megcsinált address id-t (ami nulla) átírod a megtalált, már elmentett cím ID-jére.
            $addressExists = true; //Találtam az adatbázisban címet.
        }
    }

    /* Ha nincs találat, akkor cím beszúrása és új cím ID-jének lekérése */

    /* Cím beszúrása */
    if ($addressExists == false) { //Csak akkor fut le, ha a cím nem szerepel az adatbázisban
        $address_query = "INSERT INTO `addresses`(`nationality_id`, `postal_code`, `street`) 
                    VALUES (?,?,?);";
        $address_stmt = mysqli_stmt_init($connection);

        if (mysqli_stmt_prepare($address_stmt, $address_query)) {
            mysqli_stmt_bind_param($address_stmt, "iss", $nationality, $postal, $street);
            mysqli_stmt_execute($address_stmt);
        } else {
            header("Location: ../views/create_user.php?error=address");
            echo "Address prepare error.";
        }

        /* Az imént beszúrt cím ID-jének lekérése */
        $get_add_id_query = "SELECT MAX(id) FROM addresses";
        $result = mysqli_query($connection, $get_add_id_query);
        $get_id = mysqli_fetch_assoc($result);
        $address_id = $get_id['MAX(id)'];
    }

    /* ADDRESS VALIDÁLÁS VÉGE */

    /* Statement előkészítése */
    if (mysqli_stmt_prepare($stmt, $sql_query)) {

        /* Párosítás */
        mysqli_stmt_bind_param(
            $stmt,
            'ssisssisiii',
            $fullname,
            $username,
            $id,
            $birthdate,
            $phone,
            $email,
            $address_id,
            $omid,
            $nationality,
            $language,
            $sex
        );

        /* Futtatás */
        $result = mysqli_stmt_execute($stmt);
        echo 'SQL lekérdezés, sikerült. <br> <br>';
        header("Location: ../views/create_user.php?registration=success");
    } else {
        echo "Statement: FATAL ERROR";
        header("Location: ../views/create_user.php?error=statement");
    }
} else {
    header("Location: ../views/create_user.php?cheat=true");
}