<?php
/* Elindítom a session-t, hogy tudjam használni. */
session_start();

/* User / Jelszó => Rick / Asdf1234! */

if (isset($_POST['submit'])) {
    require_once 'database.php';

    $username = mysqli_real_escape_string($connection, $_POST['username']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);

    if (empty($username) || empty($password)) {
        header("Location: ../views/index.php?login=emptyFields");
    } else {
        $sql_query = "SELECT * FROM users 
        INNER JOIN passwords on users.password_id = passwords.id
        WHERE username = '$username'";
        $result = mysqli_query($connection, $sql_query);

        if (mysqli_num_rows($result) == 1) {
            /* Ha igen, akkor van egy ember a megfelelő username-mel. */

            /* Lekéred a user adatait */
            $user = mysqli_fetch_assoc($result);

            /* Password egyeztetés */
            if (password_verify($password, $user['password'])) {
                $_SESSION["user"] = $user['username'];
                header("Location: ../views/index.php?login=success");
            }
            else
            {
                header("Location: ../views/index.php?login=pwError");

            }            
        } else {
            echo 'Nincs ilyen user';
            header("Location: ../views/index.php?login=userNotExists");
        }
    }
} else {
     die('Don\'t cheat!!!');
}